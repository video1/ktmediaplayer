//
// AtomicCounter.hpp
//

#ifndef INCLUDE_TINY_POCO_ATOMIC_COUNTER_H
#define INCLUDE_TINY_POCO_ATOMIC_COUNTER_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"
#include "poco/Types.hpp"

#include <atomic>

NS_TINY_POCO_START

/// This class implements a simple counter, which
/// provides atomic operations that are safe to
/// use in a multithreaded environment.
///
/// Typical usage of AtomicCounter is for implementing
/// reference counting and similar functionality.
class POCO_API AtomicCounter
{
public:
    typedef Int64 ValueType; /// The underlying integer type.

    /// Creates a new AtomicCounter and initializes it to zero.
    AtomicCounter();

    /// Creates a new AtomicCounter and initializes it with
    /// the given value.
    explicit AtomicCounter(ValueType initialValue);

    /// Creates the counter by copying another one.
    AtomicCounter(const AtomicCounter& counter);

    /// Destroys the AtomicCounter.
    ~AtomicCounter();

    /// Assigns the value of another AtomicCounter.
    AtomicCounter& operator = (const AtomicCounter& counter);

    /// Assigns a value to the counter.
    AtomicCounter& operator = (ValueType value);


    /// Converts the AtomicCounter to ValueType.
    operator ValueType () const;

    /// Returns the value of the counter.
    ValueType value() const;

    /// Increments the counter and returns the result.
    ValueType operator ++ (); // prefix

    /// Increments the counter and returns the previous value.
    ValueType operator ++ (int); // postfix

    /// Decrements the counter and returns the result.
    ValueType operator -- (); // prefix

    /// Decrements the counter and returns the previous value.
    ValueType operator -- (int); // postfix

    /// Returns true if the counter is zero, false otherwise.
    bool operator ! () const;

private:
    std::atomic<int> _counter;
};


//
// inlines
//

inline AtomicCounter::operator AtomicCounter::ValueType () const
{
    return _counter.load();
}


inline AtomicCounter::ValueType AtomicCounter::value() const
{
    return _counter.load();
}


inline AtomicCounter::ValueType AtomicCounter::operator ++ () // prefix
{
    return ++_counter;
}


inline AtomicCounter::ValueType AtomicCounter::operator ++ (int) // postfix
{
    return _counter++;
}


inline AtomicCounter::ValueType AtomicCounter::operator -- () // prefix
{
    return --_counter;
}


inline AtomicCounter::ValueType AtomicCounter::operator -- (int) // postfix
{
    return _counter--;
}


inline bool AtomicCounter::operator ! () const
{
    return _counter.load() == 0;
}

NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_ATOMIC_COUNTER_H
