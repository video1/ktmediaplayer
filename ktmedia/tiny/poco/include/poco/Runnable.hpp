//
// Runnable.hpp
//

#ifndef INCLUDE_TINY_POCO_RUNNABLE_H
#define INCLUDE_TINY_POCO_RUNNABLE_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"

NS_TINY_POCO_START

/// The Runnable interface with the run() method
/// must be implemented by classes that provide
/// an entry point for a thread.
class POCO_API Runnable
{
public:
    Runnable() = default;

    virtual ~Runnable() = default;;

    /// Do whatever the thread needs to do. Must
    /// be overridden by subclasses.
    virtual void run() = 0;
};

NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_RUNNABLE_H
