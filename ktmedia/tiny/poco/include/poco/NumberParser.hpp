//
// NumberParser.hpp
//
#ifndef INCLUDE_TINY_POCO_NUMBER_PARSER_H
#define INCLUDE_TINY_POCO_NUMBER_PARSER_H

#include "poco/Namespace.hpp"
#include "poco/Export.hpp"
#include "poco/Platform.hpp"
#include "poco/Types.hpp"

#include <string>

#undef min
#undef max
#include <limits>


NS_TINY_POCO_START

/// The NumberParser class provides static methods
/// for parsing numbers out of strings.
///
/// Note that leading or trailing whitespace is not allowed
/// in the string. Poco::trim() or Poco::trimInPlace()
/// can be used to remove leading or trailing whitespace.
class POCO_API NumberParser
{
public:
    static const unsigned short NUM_BASE_OCT = 010;
    static const unsigned short NUM_BASE_DEC = 10;
    static const unsigned short NUM_BASE_HEX = 0x10;

    /// Parses an integer value in decimal notation from the given string.
    /// Throws a SyntaxException if the string does not hold a number in decimal notation.
    static int parse(const std::string& s, char thousandSeparator = ',');


    /// Parses an integer value in decimal notation from the given string.
    /// Returns true if a valid integer has been found, false otherwise.
    /// If parsing was not successful, value is undefined.
    static bool tryParse(const std::string& s, int& value, char thousandSeparator = ',');


    /// Parses an unsigned integer value in decimal notation from the given string.
    /// Throws a SyntaxException if the string does not hold a number in decimal notation.
    static unsigned parseUnsigned(const std::string& s, char thousandSeparator = ',');


    /// Parses an unsigned integer value in decimal notation from the given string.
    /// Returns true if a valid integer has been found, false otherwise.
    /// If parsing was not successful, value is undefined.
    static bool tryParseUnsigned(const std::string& s, unsigned& value, char thousandSeparator = ',');


    /// Parses an integer value in hexadecimal notation from the given string.
    /// Throws a SyntaxException if the string does not hold a number in
    /// hexadecimal notation.
    static unsigned parseHex(const std::string& s);


    /// Parses an unsigned integer value in hexadecimal notation from the given string.
    /// Returns true if a valid integer has been found, false otherwise.
    /// If parsing was not successful, value is undefined.
    static bool tryParseHex(const std::string& s, unsigned& value);


    /// Parses an integer value in octal notation from the given string.
    /// Throws a SyntaxException if the string does not hold a number in
    /// hexadecimal notation.
    static unsigned parseOct(const std::string& s);


    /// Parses an unsigned integer value in octal notation from the given string.
    /// Returns true if a valid integer has been found, false otherwise.
    /// If parsing was not successful, value is undefined.
    static bool tryParseOct(const std::string& s, unsigned& value);

#if defined(POCO_HAVE_INT64)

    /// Parses a 64-bit integer value in decimal notation from the given string.
    /// Throws a SyntaxException if the string does not hold a number in decimal notation.
    static Int64 parse64(const std::string& s, char thousandSeparator = ',');


    /// Parses a 64-bit integer value in decimal notation from the given string.
    /// Returns true if a valid integer has been found, false otherwise.
    /// If parsing was not successful, value is undefined.
    static bool tryParse64(const std::string& s, Int64& value, char thousandSeparator = ',');


    /// Parses an unsigned 64-bit integer value in decimal notation from the given string.
    /// Throws a SyntaxException if the string does not hold a number in decimal notation.
    static UInt64 parseUnsigned64(const std::string& s, char thousandSeparator = ',');


    /// Parses an unsigned 64-bit integer value in decimal notation from the given string.
    /// Returns true if a valid integer has been found, false otherwise.
    /// If parsing was not successful, value is undefined.
    static bool tryParseUnsigned64(const std::string& s, UInt64& value, char thousandSeparator = ',');


    /// Parses a 64 bit-integer value in hexadecimal notation from the given string.
    /// Throws a SyntaxException if the string does not hold a number in hexadecimal notation.
    static UInt64 parseHex64(const std::string& s);


    /// Parses an unsigned 64-bit integer value in hexadecimal notation from the given string.
    /// Returns true if a valid integer has been found, false otherwise.
    /// If parsing was not successful, value is undefined.
    static bool tryParseHex64(const std::string& s, UInt64& value);


    /// Parses a 64 bit-integer value in octal notation from the given string.
    /// Throws a SyntaxException if the string does not hold a number in hexadecimal notation.
    static UInt64 parseOct64(const std::string& s);


    /// Parses an unsigned 64-bit integer value in octal notation from the given string.
    /// Returns true if a valid integer has been found, false otherwise.
    /// If parsing was not successful, value is undefined.
    static bool tryParseOct64(const std::string& s, UInt64& value);

#endif // defined(POCO_HAVE_INT64)

    /// Parses a double value in decimal floating point notation
    /// from the given string.
    /// Throws a SyntaxException if the string does not hold a floating-point
    /// number in decimal notation.
    // static double parseFloat(const std::string& s, char decimalSeparator = '.', char thousandSeparator = ',');

    /// Parses a double value in decimal floating point notation
    /// from the given string.
    /// Returns true if a valid floating point number has been found,
    /// false otherwise.
    /// If parsing was not successful, value is undefined.
    // static bool tryParseFloat(const std::string& s, double& value, char decimalSeparator = '.', char thousandSeparator = ',');


    /// Parses a bool value in decimal or string notation
    /// from the given string.
    /// Valid forms are: "0", "1", "true", "on", false", "yes", "no", "off".
    /// String forms are NOT case sensitive.
    /// Throws a SyntaxException if the string does not hold a valid bool number
    static bool parseBool(const std::string& s);


    /// Parses a bool value in decimal or string notation
    /// from the given string.
    /// Valid forms are: "0", "1", "true", "on", false", "yes", "no", "off".
    /// String forms are NOT case sensitive.
    /// Returns true if a valid bool number has been found,
    /// false otherwise.
    /// If parsing was not successful, value is undefined.
    static bool tryParseBool(const std::string& s, bool& value);
};


NS_TINY_POCO_END


#endif//INCLUDE_TINY_POCO_NUMBER_PARSER_H
