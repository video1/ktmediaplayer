//
// Checksum.hpp
//

#ifndef INCLUDE_TINY_POCO_CHECK_SUM_H
#define INCLUDE_TINY_POCO_CHECK_SUM_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"
#include "poco/Types.hpp"

#include <string>

NS_TINY_POCO_START

/// This class calculates CRC-32 or Adler-32 checksums
/// for arbitrary data.
///
/// A cyclic redundancy check (CRC) is a type of hash function, which is used to produce a
/// small, fixed-size checksum of a larger block of data, such as a packet of network
/// traffic or a computer file. CRC-32 is one of the most commonly used CRC algorithms.
///
/// Adler-32 is a checksum algorithm which was invented by Mark Adler.
/// It is almost as reliable as a 32-bit cyclic redundancy check for protecting against
/// accidental modification of data, such as distortions occurring during a transmission,
/// but is significantly faster to calculate in software.
class POCO_API Checksum
{
public:
    enum Type
    {
        TYPE_ADLER32 = 0,
        TYPE_CRC32
    };

    /// Creates a CRC-32 checksum initialized to 0.
    Checksum();

    /// Creates the Checksum, using the given type.
    Checksum(Type t);

    /// Destroys the Checksum.
    ~Checksum();

    /// Updates the checksum with the given data.
    void update(const char *data, unsigned length);

    /// Updates the checksum with the given data.
    void update(const std::string& data);

    /// Updates the checksum with the given data.
    void update(char data);

    /// Returns the calculated checksum.
    TINY_POCO_NS_STR::UInt32 checksum() const;

    /// Which type of checksum are we calulcating
    Type type() const;


private:
    Type                     _type;
    TINY_POCO_NS_STR::UInt32 _value;
};


//
// inlines
//
inline void Checksum::update(const std::string& data)
{
    update(data.c_str(), static_cast<unsigned int>(data.size()));
}


inline void Checksum::update(char c)
{
    update(&c, 1);
}


inline TINY_POCO_NS_STR::UInt32 Checksum::checksum() const
{
    return _value;
}


inline Checksum::Type Checksum::type() const
{
    return _type;
}


NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_CHECK_SUM_H
