//
// Thread_posix.h
//

#ifndef INCLUDE_TINY_POCO_THREAD_POSIX_H
#define INCLUDE_TINY_POCO_THREAD_POSIX_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"
#include "poco/Runnable.hpp"
#include "poco/Event.hpp"
#include "poco/AutoPtr.hpp"
#include "poco/SharedPtr.hpp"
#include "poco/RefCountedObject.hpp"

#include <pthread.h>
// must be limits.h (not <climits>) for PTHREAD_STACK_MIN on Solaris
#include <limits.h>
#include <errno.h>


NS_TINY_POCO_START

class POCO_API ThreadImpl
{
public:
    typedef void (*Callable)(void *);
    using TIDImpl = pthread_t;

    enum Priority
    {
        PRIO_LOWEST_IMPL,
        PRIO_LOW_IMPL,
        PRIO_NORMAL_IMPL,
        PRIO_HIGH_IMPL,
        PRIO_HIGHEST_IMPL
    };

    enum Policy
    {
        POLICY_DEFAULT_IMPL = SCHED_OTHER
    };

    ThreadImpl();
    ~ThreadImpl();

    TIDImpl tidImpl() const;

    void setPriorityImpl(int prio);
    int getPriorityImpl() const;

    void setOSPriorityImpl(int prio, int policy = SCHED_OTHER);
    int getOSPriorityImpl() const;

    static int getMinOSPriorityImpl(int policy);
    static int getMaxOSPriorityImpl(int policy);

    void setStackSizeImpl(int size);
    int getStackSizeImpl() const;

    void startImpl(TINY_POCO_NS_STR::SharedPtr<Runnable> pTarget);

    void joinImpl();
    bool joinImpl(long milliseconds);

    bool isRunningImpl() const;

    static void sleepImpl(long milliseconds);

    static void yieldImpl();

    static ThreadImpl *currentImpl();
    static TIDImpl currentTidImpl();

protected:
    static void *runnableEntry(void *pThread);
    static int   mapPrio(int prio, int policy = SCHED_OTHER);
    static int   reverseMapPrio(int osPrio, int policy = SCHED_OTHER);

private:
    class CurrentThreadHolder
    {
    public:
        CurrentThreadHolder()
        {
            if (pthread_key_create(&_key, nullptr))
                throw SystemException("cannot allocate thread context key");
        }
        ~CurrentThreadHolder()
        {
            pthread_key_delete(_key);
        }
        ThreadImpl *get() const
        {
            return reinterpret_cast<ThreadImpl *>(pthread_getspecific(_key));
        }
        void set(ThreadImpl *pThread)
        {
            pthread_setspecific(_key, pThread);
        }

    private:
        pthread_key_t _key{};
    };

    struct ThreadData: public RefCountedObject
    {
        ThreadData():
            thread(0),
            prio(PRIO_NORMAL_IMPL),
            osPrio(),
            policy(SCHED_OTHER),
            done(static_cast<Event::EventType>(false)),
            stackSize(0),
            started(false),
            joined(false)
        {
        }

        SharedPtr<Runnable> pRunnableTarget;
        pthread_t     thread;
        int           prio;
        int           osPrio;
        int           policy;
        Event         done;
        std::size_t   stackSize; //Zero means OS default
        bool          started;
        bool          joined;
    };

    AutoPtr<ThreadData> _pData;

    static CurrentThreadHolder _currentThreadHolder;
};


//
// inlines
//
inline int ThreadImpl::getPriorityImpl() const
{
    return _pData->prio;
}


inline int ThreadImpl::getOSPriorityImpl() const
{
    return _pData->osPrio;
}


inline bool ThreadImpl::isRunningImpl() const
{
    return !_pData->pRunnableTarget.isNull();
}


inline void ThreadImpl::yieldImpl()
{
    sched_yield();
}


inline int ThreadImpl::getStackSizeImpl() const
{
    return static_cast<int>(_pData->stackSize);
}


inline ThreadImpl::TIDImpl ThreadImpl::tidImpl() const
{
    return _pData->thread;
}


NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_THREAD_POSIX_H
