//
// Debugger.hpp
//

#ifndef INCLUDE_TINY_POCO_DEBUGGER_H
#define INCLUDE_TINY_POCO_DEBUGGER_H

#include "poco/Namespace.hpp"
#include "poco/Export.hpp"

#include <string>

NS_TINY_POCO_START


/// The Debugger class provides an interface to the debugger.
/// The presence of a debugger can be checked for,
/// messages can be written to the debugger's log window
/// and a break into the debugger can be enforced.
/// The methods only work if the program is compiled
/// in debug mode (the macro _DEBUG is defined).
class POCO_API Debugger
{
public:
    static bool isAvailable();

    /// Writes a message to the debugger log, if available, otherwise to
    /// standard error output.
    static void message(const std::string& msg);


    /// Writes a message to the debugger log, if available, otherwise to
    /// standard error output.
    static void message(const std::string& msg, const char *file, int line);


    /// Breaks into the debugger, if it is available.
    /// On Windows, this is done using the DebugBreak() function.
    /// On Unix, the SIGINT signal is raised.
    static void enter();


    /// Writes a debug message to the debugger log and breaks into it.
    static void enter(const std::string& msg);


    /// Writes a debug message to the debugger log and breaks into it.
    static void enter(const std::string& msg, const char *file, int line);


    /// Writes a debug message to the debugger log and breaks into it.
    static void enter(const char *file, int line);

};


NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_DEBUGGER_H
