//
// Timespan.hpp
//

#ifndef INCLUDE_TINY_POCO_TIME_SPAN_H
#define INCLUDE_TINY_POCO_TIME_SPAN_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"
#include "poco/Timestamp.hpp"

NS_TINY_POCO_START

/// A class that represents time spans up to microsecond resolution.
class POCO_API Timespan
{
public:
    using TimeDiff = Timestamp::TimeDiff;

    /// Creates a zero Timespan.
    Timespan();

    /// Creates a Timespan.
    Timespan(TimeDiff microseconds);

    /// Creates a Timespan. Useful for creating
    /// a Timespan from a struct timeval.
    Timespan(long seconds, long microseconds);


    /// Creates a Timespan.
    Timespan(int days, int hours, int minutes, int seconds, int microSeconds);


    Timespan(const Timespan& timespan);
    /// Creates a Timespan from another one.

    /// Destroys the Timespan.
    ~Timespan();

    /// Assignment operator.
    Timespan& operator = (const Timespan& timespan);

    /// Assignment operator.
    Timespan& operator = (TimeDiff microseconds);

    /// Assigns a new span.
    Timespan& assign(int days, int hours, int minutes, int seconds, int microSeconds);

    /// Assigns a new span. Useful for assigning
    /// from a struct timeval.
    Timespan& assign(long seconds, long microseconds);

    /// Swaps the Timespan with another one.
    void swap(Timespan& timespan);


    bool operator == (const Timespan& ts) const;
    bool operator != (const Timespan& ts) const;
    bool operator >  (const Timespan& ts) const;
    bool operator >= (const Timespan& ts) const;
    bool operator <  (const Timespan& ts) const;
    bool operator <= (const Timespan& ts) const;

    bool operator == (TimeDiff microSeconds) const;
    bool operator != (TimeDiff microSeconds) const;
    bool operator >  (TimeDiff microSeconds) const;
    bool operator >= (TimeDiff microSeconds) const;
    bool operator <  (TimeDiff microSeconds) const;
    bool operator <= (TimeDiff microSeconds) const;

    Timespan operator + (const Timespan& d) const;
    Timespan operator - (const Timespan& d) const;
    Timespan& operator += (const Timespan& d);
    Timespan& operator -= (const Timespan& d);

    Timespan operator + (TimeDiff microSeconds) const;
    Timespan operator - (TimeDiff microSeconds) const;
    Timespan& operator += (TimeDiff microSeconds);
    Timespan& operator -= (TimeDiff microSeconds);

    /// Returns the number of days.
    int days() const;

    /// Returns the number of hours (0 to 23).
    int hours() const;

    /// Returns the total number of hours.
    int totalHours() const;

    /// Returns the number of minutes (0 to 59).
    int minutes() const;

    /// Returns the total number of minutes.
    int totalMinutes() const;

    /// Returns the number of seconds (0 to 59).
    int seconds() const;

    /// Returns the total number of seconds.
    int totalSeconds() const;

    /// Returns the number of milliseconds (0 to 999).
    int milliseconds() const;

    /// Returns the total number of milliseconds.
    TimeDiff totalMilliseconds() const;

    /// Returns the fractions of a millisecond
    /// in microseconds (0 to 999).
    int microseconds() const;

    /// Returns the fractions of a second
    /// in microseconds (0 to 999999).
    int useconds() const;

    /// Returns the total number of microseconds.
    TimeDiff totalMicroseconds() const;

    static const TimeDiff MILLISECONDS; /// The number of microseconds in a millisecond.
    static const TimeDiff SECONDS;      /// The number of microseconds in a second.
    static const TimeDiff MINUTES;      /// The number of microseconds in a minute.
    static const TimeDiff HOURS;        /// The number of microseconds in a hour.
    static const TimeDiff DAYS;         /// The number of microseconds in a day.

private:
    TimeDiff _span;
};


//
// inlines
//
inline int Timespan::days() const
{
    return int(_span / DAYS);
}


inline int Timespan::hours() const
{
    return int((_span / HOURS) % 24);
}


inline int Timespan::totalHours() const
{
    return int(_span / HOURS);
}


inline int Timespan::minutes() const
{
    return int((_span / MINUTES) % 60);
}


inline int Timespan::totalMinutes() const
{
    return int(_span / MINUTES);
}


inline int Timespan::seconds() const
{
    return int((_span / SECONDS) % 60);
}


inline int Timespan::totalSeconds() const
{
    return int(_span / SECONDS);
}


inline int Timespan::milliseconds() const
{
    return int((_span / MILLISECONDS) % 1000);
}


inline Timespan::TimeDiff Timespan::totalMilliseconds() const
{
    return _span / MILLISECONDS;
}


inline int Timespan::microseconds() const
{
    return int(_span % 1000);
}


inline int Timespan::useconds() const
{
    return int(_span % 1000000);
}


inline Timespan::TimeDiff Timespan::totalMicroseconds() const
{
    return _span;
}


inline bool Timespan::operator == (const Timespan& ts) const
{
    return _span == ts._span;
}


inline bool Timespan::operator != (const Timespan& ts) const
{
    return _span != ts._span;
}


inline bool Timespan::operator >  (const Timespan& ts) const
{
    return _span > ts._span;
}


inline bool Timespan::operator >= (const Timespan& ts) const
{
    return _span >= ts._span;
}


inline bool Timespan::operator <  (const Timespan& ts) const
{
    return _span < ts._span;
}


inline bool Timespan::operator <= (const Timespan& ts) const
{
    return _span <= ts._span;
}


inline bool Timespan::operator == (TimeDiff microSeconds) const
{
    return _span == microSeconds;
}


inline bool Timespan::operator != (TimeDiff microSeconds) const
{
    return _span != microSeconds;
}


inline bool Timespan::operator >  (TimeDiff microSeconds) const
{
    return _span > microSeconds;
}


inline bool Timespan::operator >= (TimeDiff microSeconds) const
{
    return _span >= microSeconds;
}


inline bool Timespan::operator <  (TimeDiff microSeconds) const
{
    return _span < microSeconds;
}


inline bool Timespan::operator <= (TimeDiff microSeconds) const
{
    return _span <= microSeconds;
}


inline void swap(Timespan& s1, Timespan& s2)
{
    s1.swap(s2);
}


inline Timespan::~Timespan()
{
}


NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_TIME_SPAN_H
