//
// ScopedLock.hpp
//

#ifndef INCLUDE_TINY_POCO_SCOPEDLOCK_H
#define INCLUDE_TINY_POCO_SCOPEDLOCK_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"
#include "poco/Bugcheck.hpp"

NS_TINY_POCO_START


/// A class that simplifies thread synchronization
/// with a mutex.
/// The constructor accepts a Mutex (and optionally
/// a timeout value in milliseconds) and locks it.
/// The destructor unlocks the mutex.
template <typename M>
class ScopedLock
{
public:
    explicit ScopedLock(M& mutex): _mutex(mutex)
    {
        _mutex.lock();
    }

    ScopedLock(M& mutex, long milliseconds): _mutex(mutex)
    {
        _mutex.lock(milliseconds);
    }

    ~ScopedLock()
    {
        try
        {
            _mutex.unlock();
        }
        catch (...)
        {
            poco_unexpected();
        }
    }

private:
    M& _mutex;

    ScopedLock();
    ScopedLock(const ScopedLock&);
    ScopedLock& operator = (const ScopedLock&);
};


/// A class that simplifies thread synchronization
/// with a mutex.
/// The constructor accepts a Mutex (and optionally
/// a timeout value in milliseconds) and locks it.
/// The destructor unlocks the mutex.
/// The unlock() member function allows for manual
/// unlocking of the mutex.
template <typename M>
class ScopedLockWithUnlock
{
public:
    explicit ScopedLockWithUnlock(M& mutex): _pMutex(&mutex)
    {
        _pMutex->lock();
    }

    ScopedLockWithUnlock(M& mutex, long milliseconds): _pMutex(&mutex)
    {
        _pMutex->lock(milliseconds);
    }

    ~ScopedLockWithUnlock()
    {
        try
        {
            unlock();
        }
        catch (...)
        {
            poco_unexpected();
        }
    }

    void unlock()
    {
        if (_pMutex)
        {
            _pMutex->unlock();
            _pMutex = 0;
        }
    }

private:
    M *_pMutex;

public:
    ScopedLockWithUnlock() = delete;
    ScopedLockWithUnlock(const ScopedLockWithUnlock&) = delete;
    ScopedLockWithUnlock& operator = (const ScopedLockWithUnlock&) = delete;
};


NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_SCOPEDLOCK_H
