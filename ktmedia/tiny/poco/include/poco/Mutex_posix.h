//
// Mutex_posix.h
//

#ifndef INCLUDE_TINY_POCO_MUTEX_POSIX_H
#define INCLUDE_TINY_POCO_MUTEX_POSIX_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"
#include "poco/Exception.hpp"

#include <pthread.h>
#include <errno.h>

NS_TINY_POCO_START

class POCO_API MutexImpl
{
protected:
    MutexImpl();
    MutexImpl(bool fast);
    ~MutexImpl();

    void lockImpl();
    bool tryLockImpl();
    bool tryLockImpl(long milliseconds);
    void unlockImpl();

private:
    pthread_mutex_t _mutex;
};


class POCO_API FastMutexImpl: public MutexImpl
{
protected:
    FastMutexImpl();
    ~FastMutexImpl();
};


//
// inlines
//
inline void MutexImpl::lockImpl()
{
    if (pthread_mutex_lock(&_mutex))
        throw SystemException("cannot lock mutex");
}


inline bool MutexImpl::tryLockImpl()
{
    int rc = pthread_mutex_trylock(&_mutex);
    if (rc == 0)
        return true;
    else if (rc == EBUSY)
        return false;
    else
        throw SystemException("cannot lock mutex");
}


inline void MutexImpl::unlockImpl()
{
    if (pthread_mutex_unlock(&_mutex))
        throw SystemException("cannot unlock mutex");
}


NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_MUTEX_POSIX_H
