//
// Platform_posix.h
//

#ifndef INCLUDE_TINY_POCO_PLATFORM_POSIX_H
#define INCLUDE_TINY_POCO_PLATFORM_POSIX_H


//
// PA-RISC based HP-UX platforms have some issues...
//
#if defined(hpux) || defined(_hpux)
#   if defined(__hppa) || defined(__hppa__)
#       define POCO_NO_SYS_SELECT_H 1
#       if defined(__HP_aCC)
#           define POCO_NO_TEMPLATE_ICOMPARE 1
#       endif
#   endif
#endif


//
// Thread-safety of local static initialization
//
#if __cplusplus >= 201103L || __GNUC__ >= 4 || defined(__clang__)
#   ifndef POCO_LOCAL_STATIC_INIT_IS_THREADSAFE
#       define POCO_LOCAL_STATIC_INIT_IS_THREADSAFE 1
#   endif
#endif


//
// No syslog.h on QNX/BB10
//
#if defined(__QNXNTO__)
#   define POCO_NO_SYSLOGCHANNEL
#endif


#endif//INCLUDE_TINY_POCO_PLATFORM_POSIX_H
