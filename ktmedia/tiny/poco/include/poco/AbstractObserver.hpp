//
// AbstractObserver.hpp
//

#ifndef INCLUDE_TINY_POCO_ABSTRACT_OBSERVER_H
#define INCLUDE_TINY_POCO_ABSTRACT_OBSERVER_H

#include "poco/Namespace.hpp"
#include "poco/Export.hpp"
#include "poco/Notification.hpp"


NS_TINY_POCO_START

/// The base class for all instantiations of
/// the Observer and NObserver template classes.
class POCO_API AbstractObserver
{
public:
    AbstractObserver();
    AbstractObserver(const AbstractObserver& observer);
    virtual ~AbstractObserver();

    AbstractObserver& operator = (const AbstractObserver& observer);

    virtual void notify(Notification *pNf) const = 0;
    virtual bool equals(const AbstractObserver& observer) const = 0;
    virtual bool accepts(Notification *pNf) const = 0;
    virtual AbstractObserver *clone() const = 0;
    virtual void disable() = 0;
};


NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_ABSTRACT_OBSERVER_H
