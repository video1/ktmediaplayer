//
// ThreadLocal.hpp
//

#ifndef INCLUDE_TINY_POCO_THREAD_LOCAL_H
#define INCLUDE_TINY_POCO_THREAD_LOCAL_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"

#include <map>

NS_TINY_POCO_START

/// This is the base class for all objects
/// that the ThreadLocalStorage class manages.
class POCO_API TLSAbstractSlot
{
public:
    TLSAbstractSlot() = default;

    virtual ~TLSAbstractSlot() = default;
};



/// The Slot template wraps another class
/// so that it can be stored in a ThreadLocalStorage
/// object. This class is used internally, and you
/// must not create instances of it yourself.
template <class C>
class TLSSlot: public TLSAbstractSlot
{
public:
    TLSSlot(): _value()
    {
    }

    ~TLSSlot() override = default;

    C& value()
    {
        return _value;
    }

public:
    TLSSlot(const TLSSlot&) = delete;
    TLSSlot& operator = (const TLSSlot&) = delete;

private:
    C _value;
};


/// This class manages the local storage for each thread.
/// Never use this class directly, always use the
/// ThreadLocal template for managing thread local storage.
class POCO_API ThreadLocalStorage
{
public:
    /// Creates the TLS.
    ThreadLocalStorage() = default;

    /// Deletes the TLS.
    ~ThreadLocalStorage();

    /// Returns the slot for the given key.
    TLSAbstractSlot *&get(const void *key);


    /// Returns the TLS object for the current thread
    /// (which may also be the main thread).
    static ThreadLocalStorage& current();


    /// Clears the current thread's TLS object.
    /// Does nothing in the main thread.
    static void clear();

private:
    typedef std::map<const void *, TLSAbstractSlot *> TLSMap;
    TLSMap _map;

    friend class Thread;
};



NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_THREAD_LOCAL_H
