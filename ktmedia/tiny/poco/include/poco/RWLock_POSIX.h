//
// RWLock_POSIX.h
//

#ifndef INCLUDE_TINY_POCO_RW_LOCK_POSIX_H
#define INCLUDE_TINY_POCO_RW_LOCK_POSIX_H

#include "poco/Namespace.hpp"
#include "poco/Export.hpp"
#include "poco/Platform.hpp"
#include "poco/Types.hpp"
#include "poco/Exception.hpp"

#include <pthread.h>
#include <errno.h>


NS_TINY_POCO_START


class POCO_API RWLockImpl
{
protected:
    RWLockImpl();
    ~RWLockImpl();

    void readLockImpl();
    bool tryReadLockImpl();
    void writeLockImpl();
    bool tryWriteLockImpl();
    void unlockImpl();

private:
    pthread_rwlock_t _rwl;
};


//
// inlines
//
inline void RWLockImpl::readLockImpl()
{
    if (pthread_rwlock_rdlock(&_rwl))
        throw SystemException("cannot lock reader/writer lock");
}


inline bool RWLockImpl::tryReadLockImpl()
{
    int rc = pthread_rwlock_tryrdlock(&_rwl);
    if (rc == 0)
        return true;
    else if (rc == EBUSY)
        return false;
    else
        throw SystemException("cannot lock reader/writer lock");

}


inline void RWLockImpl::writeLockImpl()
{
    if (pthread_rwlock_wrlock(&_rwl))
        throw SystemException("cannot lock reader/writer lock");
}


inline bool RWLockImpl::tryWriteLockImpl()
{
    int rc = pthread_rwlock_trywrlock(&_rwl);
    if (rc == 0)
        return true;
    else if (rc == EBUSY)
        return false;
    else
        throw SystemException("cannot lock reader/writer lock");

}


inline void RWLockImpl::unlockImpl()
{
    if (pthread_rwlock_unlock(&_rwl))
        throw SystemException("cannot unlock mutex");
}




NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_RW_LOCK_POSIX_H

