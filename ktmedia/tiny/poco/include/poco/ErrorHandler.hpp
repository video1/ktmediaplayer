//
// ErrorHandler.hpp
//

#ifndef INCLUDE_TINY_POCO_ERRORHANDLE_H
#define INCLUDE_TINY_POCO_ERRORHANDLE_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"
#include "poco/Exception.hpp"
#include "poco/Mutex.hpp"

NS_TINY_POCO_START

/// This is the base class for thread error handlers.
///
/// An unhandled exception that causes a thread to terminate is usually
/// silently ignored, since the class library cannot do anything meaningful
/// about it.
///
/// The Thread class provides the possibility to register a
/// global ErrorHandler that is invoked whenever a thread has
/// been terminated by an unhandled exception.
/// The ErrorHandler must be derived from this class and can
/// provide implementations of all three exception() overloads.
///
/// The ErrorHandler is always invoked within the context of
/// the offending thread.
class POCO_API ErrorHandler
{
public:

    /// Creates the ErrorHandler.
    ErrorHandler();

    /// Destroys the ErrorHandler.
    virtual ~ErrorHandler();


    /// Called when a Poco::Exception (or a subclass)
    /// caused the thread to terminate.
    ///
    /// This method should not throw any exception - it would
    /// be silently ignored.
    ///
    /// The default implementation just breaks into the debugger.
    virtual void exception(const Exception& exc);


    /// Called when a std::exception (or a subclass)
    /// caused the thread to terminate.
    ///
    /// This method should not throw any exception - it would
    /// be silently ignored.
    ///
    /// The default implementation just breaks into the debugger.
    virtual void exception(const std::exception& exc);


    /// Called when an exception that is neither a
    /// Poco::Exception nor a std::exception caused
    /// the thread to terminate.
    ///
    /// This method should not throw any exception - it would
    /// be silently ignored.
    ///
    /// The default implementation just breaks into the debugger.
    virtual void exception();


    /// Invokes the currently registered ErrorHandler.
    static void handle(const Exception& exc);


    /// Invokes the currently registered ErrorHandler.
    static void handle(const std::exception& exc);


    /// Invokes the currently registered ErrorHandler.
    static void handle();

    /// Registers the given handler as the current error handler.
    ///
    /// Returns the previously registered handler.
    static ErrorHandler *set(ErrorHandler *pHandler);


    /// Returns a pointer to the currently registered
    /// ErrorHandler.
    static ErrorHandler *get();


protected:
    static ErrorHandler *defaultHandler();
    /// Returns the default ErrorHandler.

private:
    static ErrorHandler *_pHandler;
    static FastMutex     _mutex;
};


//
// inlines
//
inline ErrorHandler *ErrorHandler::get()
{
    return _pHandler;
}

NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_ERRORHANDLE_H
