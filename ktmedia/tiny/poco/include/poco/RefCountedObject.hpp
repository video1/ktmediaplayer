//
// RefCountedObject.hpp
//

#ifndef INCLUDE_TINY_POCO_REFCOUNTEDOBJECT_H
#define INCLUDE_TINY_POCO_REFCOUNTEDOBJECT_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"
#include "poco/AtomicCounter.hpp"
#include "poco/Bugcheck.hpp"

NS_TINY_POCO_START

/// A base class for objects that employ
/// reference counting based garbage collection.
///
/// Reference-counted objects inhibit construction
/// by copying and assignment.
class POCO_API RefCountedObject
{
public:
    /// Creates the RefCountedObject.
    /// The initial reference count is one.
    RefCountedObject();

    /// Increments the object's reference count.
    void duplicate() const;

    /// Decrements the object's reference count
    /// and deletes the object if the count
    /// reaches zero.
    void release() const noexcept;

    /// Returns the reference count.
    int referenceCount() const;


protected:
    /// Destroys the RefCountedObject.
    virtual ~RefCountedObject();


private:
    RefCountedObject(const RefCountedObject&);
    RefCountedObject& operator = (const RefCountedObject&);

    mutable AtomicCounter _counter;
};


//
// inlines
//
inline int RefCountedObject::referenceCount() const
{
    return _counter.value();
}


inline void RefCountedObject::duplicate() const
{
    ++_counter;
}


inline void RefCountedObject::release() const noexcept
{
    try
    {
        if (--_counter == 0) delete this;
    }
    catch (...)
    {
        poco_unexpected();
    }
}


NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_REFCOUNTEDOBJECT_H
