//
// RWLock_Android.h
//

#ifndef INCLUDE_TINY_POCO_RW_LOCK_ANDROID_H
#define INCLUDE_TINY_POCO_RW_LOCK_ANDROID_H

#include "poco/Namespace.hpp"
#include "poco/Export.hpp"
#include "poco/Platform.hpp"
#include "poco/Types.hpp"
#include "poco/Exception.hpp"

#include <pthread.h>
#include <errno.h>

NS_TINY_POCO_START

class POCO_API RWLockImpl
{
protected:
    RWLockImpl();
    ~RWLockImpl();

    void readLockImpl();
    bool tryReadLockImpl();
    void writeLockImpl();
    bool tryWriteLockImpl();
    void unlockImpl();

private:
    pthread_mutex_t _mutex;
};


//
// inlines
//
inline void RWLockImpl::readLockImpl()
{
    if (pthread_mutex_lock(&_mutex))
        throw SystemException("cannot lock reader/writer lock");
}


inline bool RWLockImpl::tryReadLockImpl()
{
    int rc = pthread_mutex_trylock(&_mutex);
    if (rc == 0)
        return true;
    else if (rc == EBUSY)
        return false;
    else
        throw SystemException("cannot lock reader/writer lock");
}


inline void RWLockImpl::writeLockImpl()
{
    if (pthread_mutex_lock(&_mutex))
        throw SystemException("cannot lock reader/writer lock");
}


inline bool RWLockImpl::tryWriteLockImpl()
{
    int rc = pthread_mutex_trylock(&_mutex);
    if (rc == 0)
        return true;
    else if (rc == EBUSY)
        return false;
    else
        throw SystemException("cannot lock reader/writer lock");
}


inline void RWLockImpl::unlockImpl()
{
    if (pthread_mutex_unlock(&_mutex))
        throw SystemException("cannot unlock reader/writer lock");
}

NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_RW_LOCK_ANDROID_H
