//
// foundation.hpp
//

#ifndef INCLUDE_TINY_POCO_FOUNDATION_H
#define INCLUDE_TINY_POCO_FOUNDATION_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"
#include "poco/Types.hpp"
#include "poco/Platform.hpp"
#include "poco/Bugcheck.hpp"
#include "poco/Exception.hpp"


#endif//INCLUDE_TINY_POCO_FOUNDATION_H
