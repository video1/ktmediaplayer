//
// Export.hpp
//

#ifndef INCLUDE_TINY_POCO_EXPORT_H
#define INCLUDE_TINY_POCO_EXPORT_H

#if (defined(_WIN32) || defined(_WIN32_WCE))
#   if defined(POCO_EXPORT)
#       define POCO_API __declspec(dllexport)
#   else
#       define POCO_API __declspec(dllimport)
#   endif
#endif

#if !defined(POCO_API)
#   if defined (__GNUC__) && (__GNUC__ >= 4)
#       define POCO_API __attribute__ ((visibility ("default")))
#   else
#       define POCO_API
#   endif
#endif


//
// POCO_DEPRECATED
//
// A macro expanding to a compiler-specific clause to
// mark a class or function as deprecated.
//
#if defined(POCO_NO_DEPRECATED)
#   define POCO_DEPRECATED
#elif defined(_GNUC_)
#   define POCO_DEPRECATED __attribute__((deprecated))
#elif defined(__clang__)
#   define POCO_DEPRECATED __attribute__((deprecated))
#elif defined(_MSC_VER)
#   define POCO_DEPRECATED __declspec(deprecated)
#else
#   define POCO_DEPRECATED
#endif

#endif//INCLUDE_TINY_POCO_EXPORT_H
