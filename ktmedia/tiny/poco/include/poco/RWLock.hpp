//
// RWLock.hpp
//

#ifndef INCLUDE_TINY_POCO_RW_LOCK_H
#define INCLUDE_TINY_POCO_RW_LOCK_H

#include "poco/Namespace.hpp"
#include "poco/Export.hpp"
#include "poco/Platform.hpp"
#include "poco/Types.hpp"
#include "poco/Exception.hpp"
#include "poco/Bugcheck.hpp"

#if POCO_OS == POCO_OS_ANDROID
#   include "poco/RWLock_Android.h"
#else
#   include "poco/RWLock_POSIX.h"
#endif


NS_TINY_POCO_START


class ScopedRWLock;
class ScopedReadRWLock;
class ScopedWriteRWLock;

/// A reader writer lock allows multiple concurrent
/// readers or one exclusive writer.
class POCO_API RWLock: private RWLockImpl
{
public:
    using ScopedLock      = ScopedRWLock;
    using ScopedReadLock  = ScopedReadRWLock;
    using ScopedWriteLock = ScopedWriteRWLock;

    /// Creates the Reader/Writer lock.
    RWLock();

    /// Destroys the Reader/Writer lock.
    ~RWLock();

    /// Acquires a read lock. If another thread currently holds a write lock,
    /// waits until the write lock is released.
    void readLock();

    /// Tries to acquire a read lock. Immediately returns true if successful, or
    /// false if another thread currently holds a write lock.
    bool tryReadLock();

    /// Acquires a write lock. If one or more other threads currently hold
    /// locks, waits until all locks are released. The results are undefined
    /// if the same thread already holds a read or write lock
    void writeLock();

    /// Tries to acquire a write lock. Immediately returns true if successful,
    /// or false if one or more other threads currently hold
    /// locks. The result is undefined if the same thread already
    /// holds a read or write lock.
    bool tryWriteLock();


    /// Releases the read or write lock.
    void unlock();


private:
    RWLock(const RWLock&);
    RWLock& operator = (const RWLock&);
};


/// A variant of ScopedLock for reader/writer locks.
class POCO_API ScopedRWLock
{
public:
    ScopedRWLock(RWLock& rwl, bool write = false);
    ~ScopedRWLock();

private:
    RWLock& _rwl;

    ScopedRWLock();
    ScopedRWLock(const ScopedRWLock&);
    ScopedRWLock& operator = (const ScopedRWLock&);
};


/// A variant of ScopedLock for reader locks.
class POCO_API ScopedReadRWLock : public ScopedRWLock

{
public:
    ScopedReadRWLock(RWLock& rwl);
    ~ScopedReadRWLock();
};


/// A variant of ScopedLock for writer locks.
class POCO_API ScopedWriteRWLock : public ScopedRWLock
{
public:
    ScopedWriteRWLock(RWLock& rwl);
    ~ScopedWriteRWLock();
};


//
// inlines
//
inline void RWLock::readLock()
{
    readLockImpl();
}


inline bool RWLock::tryReadLock()
{
    return tryReadLockImpl();
}


inline void RWLock::writeLock()
{
    writeLockImpl();
}


inline bool RWLock::tryWriteLock()
{
    return tryWriteLockImpl();
}


inline void RWLock::unlock()
{
    unlockImpl();
}


inline ScopedRWLock::ScopedRWLock(RWLock& rwl, bool write): _rwl(rwl)
{
    if (write)
        _rwl.writeLock();
    else
        _rwl.readLock();
}


inline ScopedRWLock::~ScopedRWLock()
{
    try
    {
        _rwl.unlock();
    }
    catch (...)
    {
        poco_unexpected();
    }
}


inline ScopedReadRWLock::ScopedReadRWLock(RWLock& rwl): ScopedRWLock(rwl, false)
{
}


inline ScopedReadRWLock::~ScopedReadRWLock()
{
}


inline ScopedWriteRWLock::ScopedWriteRWLock(RWLock& rwl): ScopedRWLock(rwl, true)
{
}


inline ScopedWriteRWLock::~ScopedWriteRWLock()
{
}




NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_RW_LOCK_H
