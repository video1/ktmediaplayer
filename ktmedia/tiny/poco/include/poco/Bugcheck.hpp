//
// Bugcheck.hpp
//

#ifndef INCLUDE_TINY_POCO_BUG_CHECK_H
#define INCLUDE_TINY_POCO_BUG_CHECK_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"

#include <string>

NS_TINY_POCO_START


class POCO_API Bugcheck
{
public:
    /// An assertion failed. Break into the debugger, if
    /// possible, then throw an AssertionViolationException.
    static void assertion(const char *cond, const char *file, int line, const char *text = 0);


    /// An null pointer was encountered. Break into the debugger, if
    /// possible, then throw an NullPointerException.
    static void nullPointer(const char *ptr, const char *file, int line);


    /// An internal error was encountered. Break into the debugger, if
    /// possible, then throw an BugcheckException.
    static void bugcheck(const char *file, int line);


    /// An internal error was encountered. Break into the debugger, if
    /// possible, then throw an BugcheckException.
    static void bugcheck(const char *msg, const char *file, int line);


    /// An exception was caught in a destructor. Break into debugger,
    /// if possible and report exception. Must only be called from
    /// within a catch () block as it rethrows the exception to
    /// determine its class.
    static void unexpected(const char *file, int line);


    /// An internal error was encountered. Break into the debugger, if possible.
    static void debugger(const char *file, int line);


    /// An internal error was encountered. Break into the debugger, if possible.
    static void debugger(const char *msg, const char *file, int line);


protected:
    static std::string what(const char *msg, const char *file, int line, const char *text = 0);
};


//
// useful macros (these automatically supply line number and file name)
//
#if defined(__KLOCWORK__) || defined(__clang_analyzer__)
#   include <cstdlib> // for abort
#   define poco_assert_dbg(cond)           do { if (!(cond)) std::abort(); } while (0)
#   define poco_assert_msg_dbg(cond, text) do { if (!(cond)) std::abort(); } while (0)
#   define poco_assert(cond)               do { if (!(cond)) std::abort(); } while (0)
#   define poco_assert_msg(cond, text)     do { if (!(cond)) std::abort(); } while (0)
#   define poco_check_ptr(ptr)             do { if (!(ptr)) std::abort(); }  while (0)
#   define poco_bugcheck()                 do { std::abort(); }              while (0)
#   define poco_bugcheck_msg(msg)          do { std::abort(); }              while (0)

#else // defined(__KLOCWORK__) || defined(__clang_analyzer__)
#   if defined(_DEBUG)
#       define poco_assert_dbg(cond) \
            if (!(cond)) TINY_POCO_NS_STR::Bugcheck::assertion(#cond, __FILE__, __LINE__); else (void) 0
#       define poco_assert_msg_dbg(cond, text) \
            if (!(cond)) TINY_POCO_NS_STR::Bugcheck::assertion(#cond, __FILE__, __LINE__, text); else (void) 0
#   else
#       define poco_assert_msg_dbg(cond, text)
#       define poco_assert_dbg(cond)
#   endif
#define poco_assert(cond) \
    if (!(cond)) TINY_POCO_NS_STR::Bugcheck::assertion(#cond, __FILE__, __LINE__); else (void) 0
#define poco_assert_msg(cond, text) \
    if (!(cond)) TINY_POCO_NS_STR::Bugcheck::assertion(#cond, __FILE__, __LINE__, text); else (void) 0
#define poco_check_ptr(ptr) \
    if (!(ptr)) TINY_POCO_NS_STR::Bugcheck::nullPointer(#ptr, __FILE__, __LINE__); else (void) 0
#define poco_bugcheck() \
    TINY_POCO_NS_STR::Bugcheck::bugcheck(__FILE__, __LINE__)
#define poco_bugcheck_msg(msg) \
    TINY_POCO_NS_STR::Bugcheck::bugcheck(msg, __FILE__, __LINE__)

#endif // defined(__KLOCWORK__) || defined(__clang_analyzer__)


#define poco_unexpected() \
    TINY_POCO_NS_STR::Bugcheck::unexpected(__FILE__, __LINE__);

#define poco_debugger() \
    TINY_POCO_NS_STR::Bugcheck::debugger(__FILE__, __LINE__)

#define poco_debugger_msg(msg) \
    TINY_POCO_NS_STR::Bugcheck::debugger(msg, __FILE__, __LINE__)

#if defined(_DEBUG)
#   define poco_stdout_dbg(outstr) \
    std::cout << __FILE__ << '(' << std::dec << __LINE__ << "):" << outstr << std::endl;
#else
#   define poco_stdout_dbg(outstr)
#endif


#if defined(_DEBUG)
#   define poco_stderr_dbg(outstr) \
        std::cerr << __FILE__ << '(' << std::dec << __LINE__ << "):" << outstr << std::endl;
#else
#   define poco_stderr_dbg(outstr)
#endif





NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_BUG_CHECK_H

