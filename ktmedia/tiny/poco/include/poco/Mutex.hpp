//
// Mutex.hpp
//

#ifndef INCLUDE_TINY_POCO_MUTEX_H
#define INCLUDE_TINY_POCO_MUTEX_H

#include "poco/Export.hpp"
#include "poco/Namespace.hpp"
#include "poco/Exception.hpp"
#include "poco/ScopedLock.hpp"
#include "poco/Timestamp.hpp"

#if __cplusplus >= 201103L
#   ifndef POCO_HAVE_STD_ATOMICS
#       define POCO_HAVE_STD_ATOMICS
#   endif
#endif

#ifdef POCO_HAVE_STD_ATOMICS
#   include <atomic>
#endif

#include "poco/Mutex_posix.h"


NS_TINY_POCO_START


/// A Mutex (mutual exclusion) is a synchronization
/// mechanism used to control access to a shared resource
/// in a concurrent (multithreaded) scenario.
/// Mutexes are recursive, that is, the same mutex can be
/// locked multiple times by the same thread (but, of course,
/// not by other threads).
/// Using the ScopedLock class is the preferred way to automatically
/// lock and unlock a mutex.
class POCO_API Mutex: private MutexImpl
{
public:
    using ScopedLock = TINY_POCO_NS_STR::ScopedLock<Mutex>;

    /// creates the Mutex.
    Mutex();

    /// destroys the Mutex.
    ~Mutex();


    /// Locks the mutex. Blocks if the mutex
    /// is held by another thread.
    void lock();

    /// Locks the mutex. Blocks up to the given number of milliseconds
    /// if the mutex is held by another thread. Throws a TimeoutException
    /// if the mutex can not be locked within the given timeout.
    ///
    /// Performance Note: On most platforms (including Windows), this member function is
    /// implemented using a loop calling (the equivalent of) tryLock() and Thread::sleep().
    /// On POSIX platforms that support pthread_mutex_timedlock(), this is used.
    void lock(long milliseconds);

    /// Tries to lock the mutex. Returns false immediately
    /// if the mutex is already held by another thread.
    /// Returns true if the mutex was successfully locked.
    bool tryLock();

    /// Locks the mutex. Blocks up to the given number of milliseconds
    /// if the mutex is held by another thread.
    /// Returns true if the mutex was successfully locked.
    ///
    /// Performance Note: On most platforms (including Windows), this member function is
    /// implemented using a loop calling (the equivalent of) tryLock() and Thread::sleep().
    /// On POSIX platforms that support pthread_mutex_timedlock(), this is used.
    bool tryLock(long milliseconds);

    /// Unlocks the mutex so that it can be acquired by
    /// other threads.
    void unlock();

private:
    Mutex(const Mutex&);
    Mutex& operator = (const Mutex&);
};


/// A FastMutex (mutual exclusion) is similar to a Mutex.
/// Unlike a Mutex, however, a FastMutex is not recursive,
/// which means that a deadlock will occur if the same
/// thread tries to lock a mutex it has already locked again.
/// Locking a FastMutex is faster than locking a recursive Mutex.
/// Using the ScopedLock class is the preferred way to automatically
/// lock and unlock a mutex.
class POCO_API FastMutex: private FastMutexImpl
{
public:
    using ScopedLock = TINY_POCO_NS_STR::ScopedLock<FastMutex>;

    /// creates the Mutex.
    FastMutex();

    /// destroys the Mutex.
    ~FastMutex();


    /// Locks the mutex. Blocks if the mutex
    /// is held by another thread.
    void lock();

    /// Locks the mutex. Blocks up to the given number of milliseconds
    /// if the mutex is held by another thread. Throws a TimeoutException
    /// if the mutex can not be locked within the given timeout.
    ///
    /// Performance Note: On most platforms (including Windows), this member function is
    /// implemented using a loop calling (the equivalent of) tryLock() and Thread::sleep().
    /// On POSIX platforms that support pthread_mutex_timedlock(), this is used.
    void lock(long milliseconds);

    /// Tries to lock the mutex. Returns false immediately
    /// if the mutex is already held by another thread.
    /// Returns true if the mutex was successfully locked.
    bool tryLock();

    /// Locks the mutex. Blocks up to the given number of milliseconds
    /// if the mutex is held by another thread.
    /// Returns true if the mutex was successfully locked.
    ///
    /// Performance Note: On most platforms (including Windows), this member function is
    /// implemented using a loop calling (the equivalent of) tryLock() and Thread::sleep().
    /// On POSIX platforms that support pthread_mutex_timedlock(), this is used.
    bool tryLock(long milliseconds);

    /// Unlocks the mutex so that it can be acquired by
    /// other threads.
    void unlock();

private:
    FastMutex(const FastMutex&);
    FastMutex& operator = (const FastMutex&);
};



#ifdef POCO_HAVE_STD_ATOMICS

/// A SpinlockMutex, implemented in terms of std::atomic_flag, as
/// busy-wait mutual exclusion.
///
/// While in some cases (eg. locking small blocks of code)
/// busy-waiting may be an optimal solution, in many scenarios
/// spinlock may not be the right choice - it is up to the user to
/// choose the proper mutex type for their particular case.
///
/// Works with the ScopedLock class.
class POCO_API SpinlockMutex
{
public:
    using ScopedLock = TINY_POCO_NS_STR::ScopedLock<SpinlockMutex>;

    /// Creates the SpinlockMutex.
    SpinlockMutex();

    /// Destroys the SpinlockMutex.
    ~SpinlockMutex();

    /// Locks the mutex. Blocks if the mutex
    /// is held by another thread.
    void lock();

    /// Locks the mutex. Blocks up to the given number of milliseconds
    /// if the mutex is held by another thread. Throws a TimeoutException
    /// if the mutex can not be locked within the given timeout.
    void lock(long milliseconds);

    /// Tries to lock the mutex. Returns immediately, false
    /// if the mutex is already held by another thread, true
    /// if the mutex was successfully locked.
    bool tryLock();

    /// Locks the mutex. Blocks up to the given number of milliseconds
    /// if the mutex is held by another thread.
    /// Returns true if the mutex was successfully locked.
    bool tryLock(long milliseconds);

    /// Unlocks the mutex so that it can be acquired by
    /// other threads.
    void unlock();

private:
    std::atomic_flag _flag = ATOMIC_FLAG_INIT;
};

#endif // POCO_HAVE_STD_ATOMICS


/// A NullMutex is an empty mutex implementation
/// which performs no locking at all. Useful in policy driven design
/// where the type of mutex used can be now a template parameter allowing the user to switch
/// between thread-safe and not thread-safe depending on his need
/// Works with the ScopedLock class
class POCO_API NullMutex
{
public:
    using ScopedLock = TINY_POCO_NS_STR::ScopedLock<NullMutex>;

    /// Creates the NullMutex.
    NullMutex() = default;

    /// Destroys the NullMutex.
    ~NullMutex()  = default;

    /// Does nothing.
    void lock()
    {
    }

    /// Does nothing.
    void lock(long)
    {
    }

    /// Does nothing and always returns true.
    bool tryLock()
    {
        return true;
    }

    /// Does nothing and always returns true.
    bool tryLock(long)
    {
        return true;
    }

    /// Does nothing.
    void unlock()
    {
    }
};


//
// inlines
//

//
// Mutex
//

inline void Mutex::lock()
{
    lockImpl();
}


inline void Mutex::lock(long milliseconds)
{
    if (!tryLockImpl(milliseconds))
        throw TimeoutException();
}


inline bool Mutex::tryLock()
{
    return tryLockImpl();
}


inline bool Mutex::tryLock(long milliseconds)
{
    return tryLockImpl(milliseconds);
}


inline void Mutex::unlock()
{
    unlockImpl();
}


//
// FastMutex
//

inline void FastMutex::lock()
{
    lockImpl();
}


inline void FastMutex::lock(long milliseconds)
{
    if (!tryLockImpl(milliseconds))
        throw TimeoutException();
}


inline bool FastMutex::tryLock()
{
    return tryLockImpl();
}


inline bool FastMutex::tryLock(long milliseconds)
{
    return tryLockImpl(milliseconds);
}


inline void FastMutex::unlock()
{
    unlockImpl();
}


#ifdef POCO_HAVE_STD_ATOMICS

//
// SpinlockMutex
//

inline void SpinlockMutex::lock()
{
    while (_flag.test_and_set(std::memory_order_acquire));
}


inline void SpinlockMutex::lock(long milliseconds)
{
    Timestamp now;
    Timestamp::TimeDiff diff(Timestamp::TimeDiff(milliseconds) * 1000);
    while (_flag.test_and_set(std::memory_order_acquire))
    {
        if (now.isElapsed(diff)) throw TimeoutException();
    }
}


inline bool SpinlockMutex::tryLock()
{
    return !_flag.test_and_set(std::memory_order_acquire);
}


inline bool SpinlockMutex::tryLock(long milliseconds)
{
    Timestamp now;
    Timestamp::TimeDiff diff(Timestamp::TimeDiff(milliseconds) * 1000);
    while (_flag.test_and_set(std::memory_order_acquire))
    {
        if (now.isElapsed(diff)) return false;
    }
    return true;
}


inline void SpinlockMutex::unlock()
{
    _flag.clear(std::memory_order_release);
}

#endif // POCO_HAVE_STD_ATOMICS


NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_MUTEX_H
