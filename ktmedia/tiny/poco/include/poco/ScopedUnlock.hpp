//
// ScopedUnlock.hpp
//

#ifndef INCLUDE_TINY_POCO_SCOPEDUNLOCK_H
#define INCLUDE_TINY_POCO_SCOPEDUNLOCK_H

#include "poco/Namespace.hpp"
#include "poco/Export.hpp"
#include "poco/Bugcheck.hpp"

NS_TINY_POCO_START

/// A class that simplifies thread synchronization
/// with a mutex.
/// The constructor accepts a Mutex and unlocks it.
/// The destructor locks the mutex.
template <class M>
class ScopedUnlock
{
public:
    inline ScopedUnlock(M& mutex, bool unlockNow = true): _mutex(mutex)
    {
        if (unlockNow) _mutex.unlock();
    }

    inline ~ScopedUnlock()
    {
        try
        {
            _mutex.lock();
        }
        catch (...)
        {
            poco_unexpected();
        }
    }

private:
    M& _mutex;

public:
    ScopedUnlock() = delete ;
    ScopedUnlock(const ScopedUnlock&) = delete ;
    ScopedUnlock& operator = (const ScopedUnlock&) = delete ;
};


NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_SCOPEDUNLOCK_H
