//
// RunnableAdapter.hpp
//

#ifndef INCLUDE_TINY_POCO_RUNNABLE_ADAPTER_H
#define INCLUDE_TINY_POCO_RUNNABLE_ADAPTER_H

#include "poco/Namespace.hpp"
#include "poco/Export.hpp"
#include "poco/Runnable.hpp"


NS_TINY_POCO_START

/// This adapter simplifies using ordinary methods as
/// targets for threads.
/// Usage:
///    RunnableAdapter<MyClass> ra(myObject, &MyObject::doSomething));
///    Thread thr;
///    thr.Start(ra);
///
/// For using a freestanding or static member function as a thread
/// target, please see the ThreadTarget class.
template <class CLASS>
class RunnableAdapter: public Runnable
{
public:
    typedef void (CLASS::*Callback)();

    RunnableAdapter() = delete;

    RunnableAdapter(CLASS& object, Callback method): _pObject(&object), _method(method)
    {
    }

    RunnableAdapter(const RunnableAdapter& ra): _pObject(ra._pObject), _method(ra._method)
    {
    }

    ~RunnableAdapter() override = default;

    RunnableAdapter& operator = (const RunnableAdapter& ra)
    {
        _pObject = ra._pObject;
        _method  = ra._method;
        return *this;
    }

    void run() override
    {
        (_pObject->*_method)();
    }

private:
    CLASS   *_pObject;
    Callback _method;
};




NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_RUNNABLE_ADAPTER_H
