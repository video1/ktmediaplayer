//
// Notification.hpp
//

#ifndef INCLUDE_TINY_POCO_NOTIFICATION_H
#define INCLUDE_TINY_POCO_NOTIFICATION_H

#include "poco/Namespace.hpp"
#include "poco/Export.hpp"
#include "poco/AutoPtr.hpp"
#include "poco/RefCountedObject.hpp"

NS_TINY_POCO_START

/// The base class for all notification classes used
/// with the NotificationCenter and the NotificationQueue
/// classes.
/// The Notification class can be used with the AutoPtr
/// template class.
class POCO_API Notification: public RefCountedObject
{
public:
    using Ptr = AutoPtr<Notification>;

    /// Creates the notification.
    Notification();

    /// Returns the name of the notification.
    /// The default implementation returns the class name.
    virtual std::string name() const;


protected:
    ~Notification() override;
};



NS_TINY_POCO_END

#endif//INCLUDE_TINY_POCO_NOTIFICATION_H

