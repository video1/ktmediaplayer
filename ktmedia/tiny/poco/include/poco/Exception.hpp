//
// Exception.hpp
//

#ifndef INCLUDE_TINY_POCO_EXCEPTION_H
#define INCLUDE_TINY_POCO_EXCEPTION_H

#include "poco/Namespace.hpp"
#include "poco/Export.hpp"

#include <stdexcept>
#include <string>

NS_TINY_POCO_START

/// This is the base class for all exceptions defined in the poco class library.
class POCO_API Exception: public std::exception
{
public:
    /// Creates an exception.
    Exception(const std::string& msg, int code = 0);

    /// Creates an exception.
    Exception(const std::string& msg, const std::string& arg, int code = 0);

    /// Creates an exception and stores a clone
    /// of the nested exception.
    Exception(const std::string& msg, const Exception& nested, int code = 0);

    /// Copy constructor.
    Exception(const Exception& exc);

    /// Destroys the exception and deletes the nested exception.
    ~Exception() noexcept;


    /// Assignment operator.
    Exception& operator = (const Exception& exc);

    /// Returns a static string describing the exception.
    virtual const char *name() const noexcept;

    /// Returns the name of the exception class.
    virtual const char *className() const noexcept;

    /// Returns a static string describing the exception.
    ///
    /// Same as name(), but for compatibility with std::exception.
    virtual const char *what() const noexcept;

    /// Returns a pointer to the nested exception, or
    /// null if no nested exception exists.
    const Exception *nested() const;

    /// Returns the message text.
    const std::string& message() const;

    /// Returns the exception code if defined.
    int code() const;

    /// Returns a string consisting of the
    /// message name and the message text.
    std::string displayText() const;

    /// Creates an exact copy of the exception.
    ///
    /// The copy can later be thrown again by
    /// invoking rethrow() on it.
    virtual Exception *clone() const;

    /// (Re)Throws the exception.
    ///
    /// This is useful for temporarily storing a
    /// copy of an exception (see clone()), then
    /// throwing it again.
    virtual void rethrow() const;


protected:
    /// Standard constructor.
    Exception(int code = 0);

    /// Sets the message for the exception.
    void message(const std::string& msg);

    /// Sets the extended message for the exception.
    void extendedMessage(const std::string& arg);

private:
    std::string _msg;
    Exception  *_pNested;
    int         _code;
};


//
// inlines
//
inline const Exception *Exception::nested() const
{
    return _pNested;
}


inline const std::string& Exception::message() const
{
    return _msg;
}


inline void Exception::message(const std::string& msg)
{
    _msg = msg;
}


inline int Exception::code() const
{
    return _code;
}


//
// Macros for quickly declaring and implementing exception classes.
// Unfortunately, we cannot use a template here because character
// pointers (which we need for specifying the exception name)
// are not allowed as template arguments.
//
#define POCO_DECLARE_EXCEPTION_CODE(API, CLS, BASE, CODE)                           \
    class API CLS: public BASE                                                      \
    {                                                                               \
    public:                                                                         \
        CLS(int code = CODE);                                                       \
        CLS(const std::string& msg, int code = CODE);                               \
        CLS(const std::string& msg, const std::string& arg, int code = CODE);       \
        CLS(const std::string& msg, const Exception& exc, int code = CODE);         \
        CLS(const CLS& exc);                                                        \
        ~CLS() noexcept;                                                            \
        CLS& operator = (const CLS& exc);                                           \
        const char* name() const noexcept;                                          \
        const char* className() const noexcept;                                     \
        TINY_POCO_NS_STR::Exception* clone() const;                                 \
        void rethrow() const;                                                       \
    };

#define POCO_DECLARE_EXCEPTION(API, CLS, BASE) POCO_DECLARE_EXCEPTION_CODE(API, CLS, BASE, 0)

#define POCO_IMPLEMENT_EXCEPTION(CLS, BASE, NAME)                                                            \
    CLS::CLS(int code): BASE(code)                                                                           \
    {                                                                                                        \
    }                                                                                                        \
    CLS::CLS(const std::string& msg, int code): BASE(msg, code)                                              \
    {                                                                                                        \
    }                                                                                                        \
    CLS::CLS(const std::string& msg, const std::string& arg, int code): BASE(msg, arg, code)                 \
    {                                                                                                        \
    }                                                                                                        \
    CLS::CLS(const std::string& msg, const TINY_POCO_NS_STR::Exception& exc, int code): BASE(msg, exc, code) \
    {                                                                                                        \
    }                                                                                                        \
    CLS::CLS(const CLS& exc): BASE(exc)                                                                      \
    {                                                                                                        \
    }                                                                                                        \
    CLS::~CLS() noexcept                                                                                     \
    {                                                                                                        \
    }                                                                                                        \
    CLS& CLS::operator = (const CLS& exc)                                                                    \
    {                                                                                                        \
        BASE::operator = (exc);                                                                              \
        return *this;                                                                                        \
    }                                                                                                        \
    const char* CLS::name() const noexcept                                                                   \
    {                                                                                                        \
        return NAME;                                                                                         \
    }                                                                                                        \
    const char* CLS::className() const noexcept                                                              \
    {                                                                                                        \
        return typeid(*this).name();                                                                         \
    }                                                                                                        \
    TINY_POCO_NS_STR::Exception* CLS::clone() const                                                          \
    {                                                                                                        \
        return new CLS(*this);                                                                               \
    }                                                                                                        \
    void CLS::rethrow() const                                                                                \
    {                                                                                                        \
        throw *this;                                                                                         \
    }

//
// Standard exception classes
//
POCO_DECLARE_EXCEPTION(POCO_API, LogicException,              Exception)
POCO_DECLARE_EXCEPTION(POCO_API, AssertionViolationException, LogicException)
POCO_DECLARE_EXCEPTION(POCO_API, NullPointerException,        LogicException)
POCO_DECLARE_EXCEPTION(POCO_API, NullValueException,          LogicException)
POCO_DECLARE_EXCEPTION(POCO_API, BugcheckException,           LogicException)
POCO_DECLARE_EXCEPTION(POCO_API, InvalidArgumentException,    LogicException)
POCO_DECLARE_EXCEPTION(POCO_API, RangeException,              LogicException)

POCO_DECLARE_EXCEPTION(POCO_API, RuntimeException,  Exception)
POCO_DECLARE_EXCEPTION(POCO_API, NotFoundException, RuntimeException)
POCO_DECLARE_EXCEPTION(POCO_API, ExistsException,   RuntimeException)
POCO_DECLARE_EXCEPTION(POCO_API, TimeoutException,  RuntimeException)
POCO_DECLARE_EXCEPTION(POCO_API, SystemException,   RuntimeException)
POCO_DECLARE_EXCEPTION(POCO_API, BadCastException,  RuntimeException)

POCO_DECLARE_EXCEPTION(POCO_API, DataException,      RuntimeException)
POCO_DECLARE_EXCEPTION(POCO_API, SyntaxException,    DataException)
POCO_DECLARE_EXCEPTION(POCO_API, URISyntaxException, SyntaxException)


// RangeException


NS_TINY_POCO_END


#endif//INCLUDE_TINY_POCO_EXCEPTION_H
