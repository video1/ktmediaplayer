//
// namespace.hpp
//

#ifndef INCLUDE_TINY_POCO_NAMESPACE_H
#define INCLUDE_TINY_POCO_NAMESPACE_H

#ifndef NS_TINY_POCO_START
#   define NS_TINY_POCO_START  namespace tiny { namespace poco {
#endif

#ifndef NS_TINY_POCO_END
#   define NS_TINY_POCO_END } }
#endif

#ifndef USING_TINY_POCO_NS
#   define USING_TINY_POCO_NS using namespace tiny::poco
#endif

#ifndef TINY_POCO_NS_STR
#   define TINY_POCO_NS_STR tiny::poco
#endif

#endif//INCLUDE_TINY_POCO_NAMESPACE_H
