//
// Event.hpp
//

#ifndef INCLUDE_TINY_POCO_EVENT_H
#define INCLUDE_TINY_POCO_EVENT_H


#include "poco/Export.hpp"
#include "poco/Namespace.hpp"
#include "Event_posix.h"

NS_TINY_POCO_START

/// An Event is a synchronization object that
/// allows one thread to signal one or more
/// other threads that a certain event
/// has happened.
/// Usually, one thread signals an event,
/// while one or more other threads wait
/// for an event to become signalled.
class POCO_API Event: private EventImpl
{
public:
    enum EventType
    {
        EVENT_MANUALRESET, /// Manual reset event
        EVENT_AUTORESET    /// Auto-reset event
    };

    /// Creates the event. If type is EVENT_AUTORESET,
    /// the event is automatically reset after
    /// a wait() successfully returns.
    explicit Event(EventType type = EVENT_AUTORESET);


    /// Destroys the event.
    ~Event();


    /// Signals the event. If autoReset is true,
    /// only one thread waiting for the event
    /// can resume execution.
    /// If autoReset is false, all waiting threads
    /// can resume execution.
    void set();

    /// Waits for the event to become signalled.
    void wait();

    /// Waits for the event to become signalled.
    /// Throws a TimeoutException if the event
    /// does not become signalled within the specified
    /// time interval.
    void wait(long milliseconds);

    /// Waits for the event to become signalled.
    /// Returns true if the event
    /// became signalled within the specified
    /// time interval, false otherwise.
    bool tryWait(long milliseconds);

    /// Resets the event to unsignalled state.
    void reset();


private:
    Event(const Event&);
    Event& operator = (const Event&);
};


//
// inlines
//
inline void Event::set()
{
    setImpl();
}


inline void Event::wait()
{
    waitImpl();
}


inline void Event::wait(long milliseconds)
{
    if (!waitImpl(milliseconds))
        throw TimeoutException();
}


inline bool Event::tryWait(long milliseconds)
{
    return waitImpl(milliseconds);
}


inline void Event::reset()
{
    resetImpl();
}

NS_TINY_POCO_END


#endif//INCLUDE_TINY_POCO_EVENT_H
