//
// RWLock.cc
//

#include "poco/RWLock.hpp"

#if POCO_OS == POCO_OS_ANDROID
#   include "RWLock_Android.cpp"
#else
#   include "RWLock_POSIX.cpp"
#endif

NS_TINY_POCO_START

RWLock::RWLock() = default;

RWLock::~RWLock() = default;

NS_TINY_POCO_END
