//
// AtomicCounter.cc
//

#include "poco/AtomicCounter.hpp"

NS_TINY_POCO_START

AtomicCounter::AtomicCounter(): _counter(0)
{
}

AtomicCounter::AtomicCounter(AtomicCounter::ValueType initialValue): _counter(initialValue)
{
}

AtomicCounter::AtomicCounter(const AtomicCounter& counter): _counter(counter.value())
{
}

AtomicCounter::~AtomicCounter() = default;

AtomicCounter& AtomicCounter::operator = (const AtomicCounter& counter)
{
    _counter.store(counter._counter.load());
    return *this;
}

AtomicCounter& AtomicCounter::operator = (AtomicCounter::ValueType value)
{
    _counter.store(value);
    return *this;
}

NS_TINY_POCO_END
