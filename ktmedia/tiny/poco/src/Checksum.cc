//
// Checksum.cc
//

#include "poco/Checksum.hpp"
#include "poco/zlib.h"

NS_TINY_POCO_START

Checksum::Checksum():
    _type(TYPE_CRC32),
    _value(crc32(0L, Z_NULL, 0))
{
}


Checksum::Checksum(Type t):
    _type(t),
    _value(0)
{
    if (t == TYPE_CRC32)
        _value = crc32(0L, Z_NULL, 0);
    else
        _value = adler32(0L, Z_NULL, 0);
}


Checksum::~Checksum() = default;


void Checksum::update(const char *data, unsigned length)
{
    if (_type == TYPE_ADLER32)
        _value = adler32(_value, reinterpret_cast<const Bytef *>(data), length);
    else
        _value = crc32(_value, reinterpret_cast<const Bytef *>(data), length);
}

NS_TINY_POCO_END
