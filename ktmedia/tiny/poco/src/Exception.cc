//
// Exception.cc
//

#include "poco/Exception.hpp"

#include <typeinfo>

NS_TINY_POCO_START

Exception::Exception(int code):
    _pNested(nullptr), _code(code)
{
}


Exception::Exception(const std::string& msg, int code):
    _msg(msg), _pNested(nullptr), _code(code)
{
}


Exception::Exception(const std::string& msg, const std::string& arg, int code):
    _msg(msg), _pNested(nullptr), _code(code)
{
    if (!arg.empty())
    {
        _msg.append(": ");
        _msg.append(arg);
    }
}


Exception::Exception(const std::string& msg, const Exception& nested, int code):
    _msg(msg), _pNested(nested.clone()), _code(code)
{
}


Exception::Exception(const Exception& exc):
    std::exception(exc),
    _msg(exc._msg),
    _code(exc._code)
{
    _pNested = exc._pNested ? exc._pNested->clone() : nullptr;
}


Exception::~Exception() noexcept
{
    delete _pNested;
}


Exception& Exception::operator = (const Exception& exc)
{
    if (&exc != this)
    {
        Exception *newPNested = exc._pNested ? exc._pNested->clone() : 0;
        delete _pNested;
        _msg     = exc._msg;
        _pNested = newPNested;
        _code    = exc._code;
    }
    return *this;
}


const char *Exception::name() const noexcept
{
    return "Exception";
}


const char *Exception::className() const noexcept
{
    return typeid(*this).name();
}


const char *Exception::what() const noexcept
{
    return name();
}


std::string Exception::displayText() const
{
    std::string txt = name();
    if (!_msg.empty())
    {
        txt.append(": ");
        txt.append(_msg);
    }
    return txt;
}


void Exception::extendedMessage(const std::string& arg)
{
    if (!arg.empty())
    {
        if (!_msg.empty()) _msg.append(": ");
        _msg.append(arg);
    }
}


Exception *Exception::clone() const
{
    return new Exception(*this);
}


void Exception::rethrow() const
{
    throw *this;
}


POCO_IMPLEMENT_EXCEPTION(LogicException,              Exception,      "Logic exception")
POCO_IMPLEMENT_EXCEPTION(AssertionViolationException, LogicException, "Assertion violation")
POCO_IMPLEMENT_EXCEPTION(NullPointerException,        LogicException, "Null pointer")
POCO_IMPLEMENT_EXCEPTION(NullValueException,          LogicException, "Null value")
POCO_IMPLEMENT_EXCEPTION(BugcheckException,           LogicException, "Bugcheck")
POCO_IMPLEMENT_EXCEPTION(InvalidArgumentException,    LogicException, "Invalid argument")
POCO_IMPLEMENT_EXCEPTION(RangeException,              LogicException, "Out of range")

POCO_IMPLEMENT_EXCEPTION(RuntimeException,  Exception,        "Runtime exception")
POCO_IMPLEMENT_EXCEPTION(NotFoundException, RuntimeException, "Not found")
POCO_IMPLEMENT_EXCEPTION(ExistsException,   RuntimeException, "Exists")
POCO_IMPLEMENT_EXCEPTION(TimeoutException,  RuntimeException, "Timeout")
POCO_IMPLEMENT_EXCEPTION(SystemException,   RuntimeException, "System exception")
POCO_IMPLEMENT_EXCEPTION(BadCastException,  RuntimeException, "Bad cast exception")

POCO_IMPLEMENT_EXCEPTION(DataException,      RuntimeException, "Data error")
POCO_IMPLEMENT_EXCEPTION(SyntaxException,    DataException,    "Syntax error")
POCO_IMPLEMENT_EXCEPTION(URISyntaxException, SyntaxException,  "Bad URI syntax")

NS_TINY_POCO_END
