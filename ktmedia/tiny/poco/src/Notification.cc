//
// Notification.cc
//

#include "poco/Notification.hpp"

NS_TINY_POCO_START

Notification::Notification() = default;

Notification::~Notification() = default;

std::string Notification::name() const
{
    return typeid(*this).name();
}

NS_TINY_POCO_END

