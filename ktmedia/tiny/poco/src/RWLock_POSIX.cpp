//
// RWLock_POSIX.cpp
//

#include "poco/RWLock_POSIX.h"

NS_TINY_POCO_START

RWLockImpl::RWLockImpl()
{
    if (pthread_rwlock_init(&_rwl, nullptr))
        throw SystemException("cannot create reader/writer lock");
}


RWLockImpl::~RWLockImpl()
{
    pthread_rwlock_destroy(&_rwl);
}

NS_TINY_POCO_END

