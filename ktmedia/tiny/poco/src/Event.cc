//
// Event.cc
//

#include "poco/Event.hpp"
#include "Event_posix.cpp"

NS_TINY_POCO_START

Event::Event(EventType type): EventImpl(type == EVENT_AUTORESET)
{
}

Event::~Event() = default;

NS_TINY_POCO_END
