
#include "poco/Mutex.hpp"
#include "Mutex_posix.cpp"


NS_TINY_POCO_START


Mutex::Mutex() = default;

Mutex::~Mutex() = default;

FastMutex::FastMutex() = default;

FastMutex::~FastMutex() = default;

#ifdef POCO_HAVE_STD_ATOMICS
SpinlockMutex::SpinlockMutex() = default;

SpinlockMutex::~SpinlockMutex() = default;
#endif // POCO_HAVE_STD_ATOMICS

NS_TINY_POCO_END
