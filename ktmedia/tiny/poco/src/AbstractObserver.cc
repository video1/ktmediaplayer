//
// AbstractObserver.cc
//

#include "poco/AbstractObserver.hpp"

NS_TINY_POCO_START

AbstractObserver::AbstractObserver() = default;

AbstractObserver::AbstractObserver(const AbstractObserver& ) = default;

AbstractObserver::~AbstractObserver() = default;

AbstractObserver& AbstractObserver::operator = (const AbstractObserver& )
{
    return *this;
}

NS_TINY_POCO_END
