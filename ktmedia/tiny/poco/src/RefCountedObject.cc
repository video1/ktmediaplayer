//
// RefCountedObject.cc
//

#include "poco/RefCountedObject.hpp"

NS_TINY_POCO_START

RefCountedObject::RefCountedObject(): _counter(1)
{
}

RefCountedObject::~RefCountedObject() = default;

NS_TINY_POCO_END
