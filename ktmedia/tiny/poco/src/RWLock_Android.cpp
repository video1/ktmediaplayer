//
// RWLock_Android.cpp
//

#include "poco/RWLock_Android.h"

NS_TINY_POCO_START

RWLockImpl::RWLockImpl()
{
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    if (pthread_mutex_init(&_mutex, &attr))
    {
        pthread_mutexattr_destroy(&attr);
        throw SystemException("cannot create mutex");
    }
    pthread_mutexattr_destroy(&attr);
}


RWLockImpl::~RWLockImpl()
{
    pthread_mutex_destroy(&_mutex);
}

NS_TINY_POCO_END
