//
// Debugger.cc
//

#if defined(__ANDROID__)
// ... android log
#endif

#include "poco/Debugger.hpp"

#include <unistd.h>
#include <signal.h>

NS_TINY_POCO_START


bool Debugger::isAvailable()
{
#if defined(_DEBUG)
    return true;
#else
    return false;
#endif
}


void Debugger::message(const std::string& msg)
{
#if defined(_DEBUG)
    std::fputs("\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n", stderr);
    std::fputs(msg.c_str(), stderr);
    std::fputs("\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n", stderr);
#   if defined(__ANDROID__)
    // todo android log
#   endif
#endif
}


void Debugger::message(const std::string& msg, const char *file, int line)
{
#if defined(_DEBUG)
    std::ostringstream str;
    str << msg << " [in file \"" << file << "\", line " << line << "]";
    message(str.str());
#endif
}


void Debugger::enter()
{
#if defined(_DEBUG)
#   if defined(POCO_OS_FAMILY_WINDOWS)
    if (isAvailable())
    {
        kill(getpid(), SIGINT);
    }
#   endif
#endif
}


void Debugger::enter(const std::string& msg)
{
#if defined(_DEBUG)
    message(msg);
    enter();
#endif
}


void Debugger::enter(const std::string& msg, const char *file, int line)
{
#if defined(_DEBUG)
    message(msg, file, line);
    enter();
#endif
}


void Debugger::enter(const char *file, int line)
{
#if defined(_DEBUG)
    message("BREAK", file, line);
    enter();
#endif
}

NS_TINY_POCO_END
