//
// ThreadLocal.cc
//

#include "poco/ThreadLocal.hpp"
#include "poco/Thread.hpp"
#include "poco/SingletonHolder.hpp"

NS_TINY_POCO_START


ThreadLocalStorage::~ThreadLocalStorage()
{
    for(auto& p : _map)
    {
        delete  p.second;
    }
}


TLSAbstractSlot *&ThreadLocalStorage::get(const void *key)
{
    auto it = _map.find(key);
    if (it == _map.end())
        return _map.insert(TLSMap::value_type(key, reinterpret_cast<TINY_POCO_NS_STR::TLSAbstractSlot *>(0))).first->second;
    else
        return it->second;
}

namespace {
static SingletonHolder<ThreadLocalStorage> sh;
}


ThreadLocalStorage& ThreadLocalStorage::current()
{
    Thread *pThread = Thread::current();
    if (pThread)
    {
        return pThread->tls();
    }
    else
    {
        return *sh.get();
    }
}


void ThreadLocalStorage::clear()
{
    Thread *pThread = Thread::current();
    if (pThread)
        pThread->clearTLS();
}

NS_TINY_POCO_END
