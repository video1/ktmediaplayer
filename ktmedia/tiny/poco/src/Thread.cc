//
// Thread.cc
//

#include "poco/Thread.hpp"
#include "poco/Mutex.hpp"
#include "poco/Exception.hpp"
#include "poco/ThreadLocal.hpp"
#include "poco/AtomicCounter.hpp"
#include "poco/SharedPtr.hpp"

#include <sstream>
#include <utility>

#include "Thread_posix.cpp"

NS_TINY_POCO_START

namespace {
class RunnableHolder: public Runnable
{
public:
    explicit RunnableHolder(Runnable& target): _target(target)
    {
    }

    ~RunnableHolder() override = default;

    void run() override
    {
        _target.run();
    }

private:
    Runnable& _target;
};


class CallableHolder: public Runnable
{
public:
    CallableHolder(Thread::Callable callable, void *pData): _callable{callable}, _pData(pData)
    {
    }

    ~CallableHolder() override = default;

    void run() override
    {
        _callable(_pData);
    }

private:
    Thread::Callable  _callable;
    void             *_pData;
};

}//namespace



Thread::Thread(): _id(uniqueId()), _name(makeName()), _pTLS(0), _event()
{
}


Thread::Thread(const std::string& name): _id(uniqueId()), _name(name), _pTLS(0), _event()
{
}


Thread::~Thread()
{
    delete _pTLS;
}


void Thread::setPriority(Priority prio)
{
    setPriorityImpl(prio);
}


Thread::Priority Thread::getPriority() const
{
    return Priority(getPriorityImpl());
}


void Thread::start(Runnable& target)
{
    startImpl( new RunnableHolder(target) );
}


void Thread::start(TINY_POCO_NS_STR::SharedPtr<Runnable> pTarget)
{
    startImpl(std::move(pTarget));
}


void Thread::start(Callable target, void *pData)
{
    SharedPtr<CallableHolder> ptr(new CallableHolder(target, pData) );
    startImpl( new CallableHolder(target, pData) );
}


void Thread::join()
{
    joinImpl();
}


void Thread::join(long milliseconds)
{
    if (!joinImpl(milliseconds))
        throw TimeoutException();
}


bool Thread::tryJoin(long milliseconds)
{
    return joinImpl(milliseconds);
}


bool Thread::trySleep(long milliseconds)
{
    Thread *pT = Thread::current();
    poco_check_ptr(pT);
    return !(pT->_event.tryWait(milliseconds));
}


void Thread::wakeUp()
{
    _event.set();
}


ThreadLocalStorage& Thread::tls()
{
    if (!_pTLS)
        _pTLS = new ThreadLocalStorage;
    return *_pTLS;
}


void Thread::clearTLS()
{
    if (_pTLS)
    {
        delete _pTLS;
        _pTLS = 0;
    }
}


std::string Thread::makeName()
{
    std::ostringstream name;
    name << '#' << _id;
    return name.str();
}


int Thread::uniqueId()
{
    static TINY_POCO_NS_STR::AtomicCounter counter;
    return ++counter;
}


void Thread::setName(const std::string& name)
{
    FastMutex::ScopedLock lock(_mutex);

    _name = name;
}


NS_TINY_POCO_END

