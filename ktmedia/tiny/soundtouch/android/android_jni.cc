//
// android_jni.cc
//

#include <jni.h>
#include <android/log.h>

#include <stdexcept>
#include <string>

#include "SoundTouch.h"
#include "WavFile.h"


#if defined(ANDROID_TAG)
#   undef  ANDROID_TAG
#   define ANDROID_TAG "sound-touch"
#else
#   define ANDROID_TAG "sound-touch"
#endif

#define EXPORT_API __attribute__((visibility ("default")))
#define BUFF_SIZE  4096

#define LOGV(...) __android_log_print((int)ANDROID_LOG_INFO, ANDROID_TAG, __VA_ARGS__)

// String for keeping possible c++ exception error messages. Notice that this isn't
// thread-safe but it's expected that exceptions are special situations that won't
// occur in several threads in parallel.
static std::string global_error_message;

// set error message to return
static void setErrorMessage(const char *message)
{
    global_error_message = message;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#ifdef _OPENMP
#include <pthread.h>
extern pthread_key_t  gomp_tls_key;
static void          *_p_gomp_tls = null_ptr;

/// Function to initialize threading for OpenMP.
///
/// This is a workaround for bug in Android NDK v10 regarding OpenMP: OpenMP works only if
/// called from the Android App main thread because in the main thread the gomp_tls storage is
/// properly set, however, Android does not properly initialize gomp_tls storage for other threads.
/// Thus if OpenMP routines are invoked from some other thread than the main thread,
/// the OpenMP routine will crash the application due to NULL pointer access on uninitialized storage.
///
/// This workaround stores the gomp_tls storage from main thread, and copies to other threads.
/// In order this to work, the Application main thread needws to call at least "getVersionString"
/// routine.
static int _init_threading(bool warn)
{
    void *ptr = pthread_getspecific(gomp_tls_key);
    LOGV("JNI thread-specific TLS storage %ld", (long)ptr);
    if (ptr == NULL)
    {
        LOGV("JNI set missing TLS storage to %ld", (long)_p_gomp_tls);
        pthread_setspecific(gomp_tls_key, _p_gomp_tls);
    }
    else
    {
        LOGV("JNI store this TLS storage");
        _p_gomp_tls = ptr;
    }
    // Where critical, show warning if storage still not properly initialized
    if ((warn) && (_p_gomp_tls == NULL))
    {
        _setErrmsg("Error - OpenMP threading not properly initialized: Call SoundTouch.getVersionString() from the App main thread!");
        return -1;
    }
    return 0;
}
#else
static int _init_threading(bool warn)
{
    // do nothing if not OpenMP build
    return 0;
}
#endif


using namespace soundtouch;

// Processes the sound file
static void process_file(SoundTouch *pSoundTouch, const char *inFileName, const char *outFileName)
{
    int        nSamples;
    int        nChannels;
    int        buffSizeSamples;
    SAMPLETYPE sampleBuffer[BUFF_SIZE];

    // open input file
    WavInFile inFile(inFileName);
    int sampleRate = inFile.getSampleRate();
    int bits       = inFile.getNumBits();
    nChannels      = inFile.getNumChannels();

    // create output file
    WavOutFile outFile(outFileName, sampleRate, bits, nChannels);

    pSoundTouch->setSampleRate(sampleRate);
    pSoundTouch->setChannels(nChannels);

    assert(nChannels > 0);
    buffSizeSamples = BUFF_SIZE / nChannels;

    // Process samples read from the input file
    while (inFile.eof() == 0)
    {
        int num;

        // Read a chunk of samples from the input file
        num      = inFile.read(sampleBuffer, BUFF_SIZE);
        nSamples = num / nChannels;

        // Feed the samples into SoundTouch processor
        pSoundTouch->putSamples(sampleBuffer, nSamples);

        // Read ready samples from SoundTouch processor & write them output file.
        // NOTES:
        // - 'receiveSamples' doesn't necessarily return any samples at all
        //   during some rounds!
        // - On the other hand, during some round 'receiveSamples' may have more
        //   ready samples than would fit into 'sampleBuffer', and for this reason
        //   the 'receiveSamples' call is iterated for as many times as it
        //   outputs samples.
        do
        {
            nSamples = pSoundTouch->receiveSamples(sampleBuffer, buffSizeSamples);
            outFile.write(sampleBuffer, nSamples * nChannels);
        }
        while (nSamples != 0);
    }

    // Now the input file is processed, yet 'flush' few last samples that are
    // hiding in the SoundTouch's internal processing pipeline.
    pSoundTouch->flush();
    do
    {
        nSamples = pSoundTouch->receiveSamples(sampleBuffer, buffSizeSamples);
        outFile.write(sampleBuffer, nSamples * nChannels);
    }
    while (nSamples != 0);
}


extern "C" EXPORT_API jstring
Java_dai_android_media_kt_audio_soundtouch_SoundTouch_getVersionString(
    JNIEnv *env, jobject thiz)
{
    LOGV("jni: SoundTouch::getVersionString()");

    // Call example SoundTouch routine
    const char *verStr = SoundTouch::getVersionString();

    /// gomp_tls storage bug workaround - see comments in _init_threading() function!
    _init_threading(false);

    int threads = 0;
    #pragma omp parallel
    {
        #pragma omp atomic
        threads ++;
    }
    LOGV("JNI thread count %d", threads);

    // return version as string
    return env->NewStringUTF(verStr);
}

extern "C" EXPORT_API jlong
Java_dai_android_media_kt_audio_soundtouch_SoundTouch_newInstance(
    JNIEnv *env, jobject thiz)
{
    //return (jlong)(new SoundTouch());
    return reinterpret_cast<jlong>( new SoundTouch() );
}

extern "C" EXPORT_API void
Java_dai_android_media_kt_audio_soundtouch_SoundTouch_deleteInstance(
    JNIEnv *env, jobject thiz, jlong handle)
{
    auto *ptr = reinterpret_cast<SoundTouch *>(handle);
    delete ptr;
}

extern "C" EXPORT_API void
Java_dai_android_media_kt_audio_soundtouch_SoundTouch_setTempo(
    JNIEnv *env, jobject thiz, jlong handle, jfloat tempo)
{
    auto *ptr = reinterpret_cast<SoundTouch *>(handle);
    ptr->setTempo(tempo);
}

extern "C" EXPORT_API void
Java_dai_android_media_kt_audio_soundtouch_SoundTouch_setPitchSemiTones(
    JNIEnv *env, jobject thiz, jlong handle, jfloat pitch)
{
    auto *ptr = reinterpret_cast<SoundTouch *>(handle);
    ptr->setPitchSemiTones(pitch);
}

extern "C" EXPORT_API void
Java_dai_android_media_kt_audio_soundtouch_SoundTouch_setSpeed(
    JNIEnv *env, jobject thiz, jlong handle, jfloat speed)
{
    auto *ptr = reinterpret_cast<SoundTouch *>(handle);
    ptr->setRate(speed);
}

extern "C" EXPORT_API jstring
Java_dai_android_media_kt_audio_soundtouch_SoundTouch_getErrorString(
    JNIEnv *env, jobject thiz)
{
    jstring result = env->NewStringUTF(global_error_message.c_str());
    global_error_message.clear();

    return result;
}

extern "C" EXPORT_API int
Java_dai_android_media_kt_audio_soundtouch_SoundTouch_processFile(
    JNIEnv *env, jobject thiz,
    jlong handle, jstring jInputFile, jstring jOutputFile)
{
    auto       *ptr        = reinterpret_cast<SoundTouch *>(handle);
    const char *inputFile  = env->GetStringUTFChars(jInputFile,  nullptr);
    const char *outputFile = env->GetStringUTFChars(jOutputFile, nullptr);

    LOGV("jni process file: input=%s, output=%s\n", inputFile, outputFile);

    /// gomp_tls storage bug workaround - see comments in _init_threading() function!
    if (_init_threading(true))
        return -1;

    try
    {
        process_file(ptr, inputFile, outputFile);
    }
    catch (const std::runtime_error& e)
    {
        const char *err = e.what();
        // An exception occurred during processing, return the error message
        LOGV("JNI exception in SoundTouch::processFile: %s", err);
        setErrorMessage(err);
        return -1;
    }


    env->ReleaseStringUTFChars(jInputFile,  inputFile);
    env->ReleaseStringUTFChars(jOutputFile, outputFile);

    return 0;
}
