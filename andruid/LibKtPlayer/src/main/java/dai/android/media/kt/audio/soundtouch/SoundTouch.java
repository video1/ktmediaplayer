package dai.android.media.kt.audio.soundtouch;

public final class SoundTouch {

    public native static String getVersionString();

    private native void setTempo(long handle, float tempo);

    private native void setPitchSemiTones(long handle, float pitch);

    private native void setSpeed(long handle, float speed);

    private native int processFile(long handle, String inputFile, String outputFile);

    private native static long newInstance();

    private native void deleteInstance(long handle);

    public native static String getErrorString();


    private long mNativeHandle = 0L;

    public SoundTouch() {
        mNativeHandle = newInstance();
    }

    public void setTempo(float tempo) {
        setTempo(mNativeHandle, tempo);
    }


    public void setPitchSemiTones(float pitch) {
        setPitchSemiTones(mNativeHandle, pitch);
    }


    public void setSpeed(float speed) {
        setSpeed(mNativeHandle, speed);
    }


    public int processFile(String inputFile, String outputFile) {
        return processFile(mNativeHandle, inputFile, outputFile);
    }

    public void release() {
        deleteInstance(mNativeHandle);
    }


    // Load the native library upon startup
    static {
        System.loadLibrary("TinySoundTouch");
    }
}
